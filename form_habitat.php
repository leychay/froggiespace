<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/habitat.class.php');

if (isset($_GET['hid'])) {
    $is_update = TRUE;
    $habitatid = $_GET['hid'];
} else {
    $is_update = FALSE;
}

$CHabitat = new Habitat();

//process the record
if (isset($_POST['create'])) {
    $CHabitat->add_habitat();
}

//update the record
if (isset($_POST['update'])) {
    $CHabitat->update_habitat($habitatid);
}

if ($is_update) {
    //get habitat detail using ID
    $habitat = $CHabitat->get_habitat($habitatid);

    //redirect to Manage page if no record
    if (empty($habitat)) {
        set_flash_message('info', 'That habitat is not exists in Froggie\'s List');
        redirect('manage_habitat.php');
    }
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ($is_update) ? 'Update Habitat Information' : 'Add New Habitat' ; ?></h1>
    </div>
</div>

<?php echo flash_message(); ?>

<form method="POST" action="<?php echo ($is_update) ? $_SERVER['PHP_SELF'] . '?hid=' . $habitatid : $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" role="form" class="validate">
<div class="row">
    <div class="col-lg-6">

        <div class="form-group">
            <label for="name">Habitat Name</label>
            <input id="name" type="text" name="name" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Habitat Name" value="<?php echo ($is_update) ? $habitat['habitatname'] : NULL ; ?>" required autofocus />
        </div>

        <div class="form-group">
            <label for="desc">Description</label>
            <textarea id="desc" type="text" name="desc" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Habitat's description"><?php echo ($is_update) ? $habitat['desc'] : NULL ; ?></textarea>
        </div>

        <div class="form-group">
            <?php
            if ($is_update) {
                ?>
                <div class="form-group">
                    <div class="checkbox">
                        <label for="closed">
                            <input id="closed" type="checkbox" name="closed" <?php echo (! $habitat['status']) ? 'checked' : NULL ; ?> />Closed
                        </label>
                    </div>
                </div>
                <input id="update" type="submit" name="update" value="update this Habitat" class="btn btn-primary"  />


                <?php
            } else {
                ?>
                <input id="create" type="submit" name="create" value="Add New Habitat!" class="btn btn-primary"  />
                <?php
            }
            ?>
        </div>
    </div>

    <div class="col-lg-6">

        <div class="form-group">
        <label for="photo">Upload Photo</label>
        <input id="photo" type="file" name="photo" data-toggle="tooltip" data-placement="top" title="Select photo for upload" />
        <p class="help-block"><small>(only .jpg or .png is allowed) | (Maximum File Size is <?php echo ini_get('upload_max_filesize');?>)</small></p>
        </div>

        <?php if ($is_update && ! empty($habitat['photo'])) { ?>
        <div class="row text-center">
            <div class="col-md-9">
                <div class="panel panel-primary">
                <div class="panel-body">
                    <img width="300px" class="thumb" src="<?php echo ASSETPATH . '/img/' . $habitat['photo']; ?>" />
                </div>
                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
</form>

<?php require_once('include/footer.php'); ?>
