<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/frog.class.php');
require_once('class/habitat.class.php');

if (isset($_GET['hid'])) {
    $habitatid = $_GET['hid'];
}

$CFrog = new Frog();
$CHabitat = new Habitat();

//get list of Frogs that resides in this habitat
$frogs = $CFrog->get_list_frogs($habitatid);

//get Habitat detail using ID
$habitat = $CHabitat->get_habitat($habitatid);

//redirect to Manage page if no record
if (empty($habitat)) {
    set_flash_message('info', 'That habitat is not exists in Froggie\'s List');
    redirect('manage_habitat.php');
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo $habitat['habitatname'] . '\'s Profile' ; ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-center">
        <div class="">
            <?php if (! empty($habitat['photo'])) { ?>
                <img width="400px" class="thumb" src="<?php echo ASSETPATH . 'img/' . $habitat['photo']; ?>" />
            <?php
                } else {
                    echo '<p class="text-center">No Photo Available</p>';
                }
            ?>
        </div>
    </div>
</div>

<hr />

<div class="row">
    <div class="col-lg-8">
        <h4>About</h4>
        <p><?php echo nl2br($habitat['desc']); ?></p>
    </div>
    <div class="col-lg-4">
        <h4>&nbsp;</h4>
        <p>Status: &nbsp;
            <?php
            if ($habitat['status']) {
                echo '<label class="btn btn-success btn-xs">Open</label>';
            } else {
                echo '<label class="btn btn-danger btn-xs">Closed</label>';
            }

            echo '<br/>Frogs: &nbsp;';
            $list = array();
            if (! empty($frogs)) {
                foreach($frogs as $fid => $frog) {
                    $list[] = '<a href="frog.php?fid=' . $fid . '">' . $frog . '</a>';
                }

                echo implode(', ', $list);

            } else {
                echo 'N/A';
            }
            ?>
        </p>

    </div>
</div>
<?php require_once('include/footer.php'); ?>
