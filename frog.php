<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/frog.class.php');
require_once('class/habitat.class.php');

if (isset($_GET['fid'])) {
    $frogid = $_GET['fid'];
}

$CFrog = new Frog();
$CHabitat = new Habitat();

$habitat = $CHabitat->get_list_habitat();

//get Frog detail using ID
$frog = $CFrog->get_frogs($frogid);

//redirect to Manage page if no record
if (empty($frog)) {
    set_flash_message('info', 'That frog is not exists in Froggie\'s List');
    redirect('manage_frog.php');
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo $frog['frogname'] . '\'s Profile' ; ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 text-center">
        <div class="panel panel-primary">
            <?php if (! empty($frog['photo'])) { ?>
                <img width="200px" class="thumb" src="<?php echo ASSETPATH . 'img/' . $frog['photo']; ?>" />
                <?php } else {
                    echo '<p class="text-center">No Photo Available</p>';
                } ?>

            </div>
        </div>

        <div class="col-lg-8">
            <p>Gender: <?php echo ($frog['gender'] == 'm') ? 'Male' : 'Female' ; ?></p>
            <p>D.O.B: <?php echo date('d/m/Y', strtotime($frog['dob'])); ?></p>
            <p>Habitat: <a href="habitat.php?hid=<?php echo $frog['habitatid']; ?>"><?php echo (array_key_exists($frog['habitatid'], $habitat)) ? $habitat[$frog['habitatid']] : 'N/A' ; ?></a></p>
            <p>
                <?php
                if ($frog['alive']) {
                    echo '<label class="btn btn-success btn-s">Alive</label>';
                } else {
                    echo '<label class="btn btn-danger btn-s">Deceased</label>';
                }
                ?>

            </p>
        </div>
    </div>
    <?php require_once('include/footer.php'); ?>
