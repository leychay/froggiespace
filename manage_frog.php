<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/database.class.php');
require_once('class/frog.class.php');
require_once('class/habitat.class.php');

$db = new Database();

$CFrog = new Frog();
$CHabitat = new Habitat();

$frogs = $CFrog->get_frogs();
$habitat = $CHabitat->get_list_habitat();

//get the frog id for deletion
if (isset($_GET['dfid'])) {
    $CFrog->delete_frog($_GET['dfid']);
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Frogs</h1>
    </div>
</div>

<?php echo flash_message(); ?>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" role="form">
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
            <thead>
                <th>Name</th>
                <th>Gender</th>
                <th>DOB</th>
                <th>Habitat</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
            </thead>
            <tbody>
                <?php if (! empty($frogs)){ ?>
                    <?php foreach($frogs as $frog){ ?>
                    <tr <?php echo (! $frog['alive']) ? 'style="text-decoration:line-through;"' : NULL ; ?>>
                        <td><?php echo $frog['frogname']; ?></td>
                        <td><?php echo ($frog['gender'] == 'm') ? 'Male' : 'Female' ; ?></td>
                        <td><?php echo date('d/m/Y', strtotime($frog['dob'])); ?></td>
                        <td><?php echo (array_key_exists($frog['habitatid'], $habitat)) ? $habitat[$frog['habitatid']] : 'N/A'; ?></td>
                        <td class="text-center">
                            <?php
                            if ($frog['alive']) {
                                echo '<label class="btn btn-success btn-xs">Alive</label>';
                            } else {
                                echo '<label class="btn btn-danger btn-xs">Deceased</label>';
                            }
                            ?>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li class="text-center"><a href="frog.php?fid=<?php echo $frog['frogid'] ?>">View Profile</a></li>
                                    <li class="text-center"><a href="form_frog.php?fid=<?php echo $frog['frogid'] ?>">Update Info</a></li>
                                    <li class="text-center"><a class='btn-danger delete_frog' href="manage_frog.php?dfid=<?php echo $frog['frogid'] ?>">Froggie Hate'em</a></li>
                                </ul>
                            </div>

                        </td>
                    </tr>
                <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">No Frogs available!</td>
                    </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</form>

<script type="text/javascript">
$(function() {

    $('.delete_frog').click( function() {
        if (confirm("Help Froggie to remove this fellow from his friend list?")) {
            return true;
        } else {
            return false;
        }
    } );
});

</script>
<?php require_once('include/footer.php'); ?>
