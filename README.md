# FroggieSpace

The objective of this project is to develop a simple, documented, and easy-to-setup application to manage Frogs and their habitat.

## Features

* Automated Database setup
* CRUD operation for frogs and habitat

## Technologies

* Good ol' native PHP code
* Bootstrap - Responsive layout

## Requirement

* PHP 5.5.0 or above.

## Installation

    $ git clone https://bitbucket.org/leychay/froggiespace.git

#### Database setup

Since FroggieSpace already have a Database setup script pre-installed, you just have to create a new empty database, i.e: 'froggiedb':

    mysql> create database froggiedb;

### Application setup

FroggieSpace doesn't use any technology. So, cloning the application in your **`/www/`** (if you're using WAMP/LAMP stack), or, creating a typical VHost entry in your apache web server will be enough fo it to get up and running

## Usage

Fire up the web browser, and head over to the configured URL, for example using WAMP/LAMP stack: [http://localhost/froggiespace](http://localhost/froggiespace), or VHost [http://sandbox.froggiespace](http://sandbox.froggiespace)

You will be greeted with a Database setup page. Continue from there.
