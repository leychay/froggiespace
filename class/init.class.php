<?php
/**
 * Class: Initialization
 * To initialize Database Configuration file, as well as all the tables for FrogPond
 */
class Init {

    public function __construct() {

    }

    public function check_db_conf() {

        $data = array();

        if (! file_exists('db.conf.php')) {
            $status = FALSE;
            $message =  '<h3>No DB configuration file exists!</h3>';
        } else {
            $status = TRUE;
            $message = '<h2>Welcome to FroggieSpace!</h2>';
            $message .= '<h3>Froggie is in a dire need to keep track of all of his friends and his chilling spots. Help him will ya?</h3>';
        }

        $data['status'] = $status;
        $data['message'] = $message;

        return $data;

    }

    public function create_db_conf() {

        //remove submit button from the array
        unset($_POST['create']);

        //do server-side validation to ensure all required values are being entered
        $missing_fields = array();
        foreach ($_POST as $field => $value) {
            //bypass dbpass during validation since we allowed NULL value for dbpass
            if ($field != 'dbpass') {
                if (empty($value)) {
                    $missing_fields[] = $field;
                }
            }
        }

        if (! empty($missing_fields)) {

            //Show error message
            $error_message = 'Sorry! Froggie cannot create a DB Config File for you since you didn\'t fill up the required fields as requested. Missing field(s) as listed below:';
            $error_message .= '<ul><li>';
            $error_message .= implode('</li><li>', $missing_fields);
            $error_message .= '</li></ul>';

            log_message('error', 'Unable to create DB Config File due to missing field data');

            set_flash_message('danger', $error_message);
            redirect(SITEPATH);

        } else {

            //test the connection before creating the config file to avoid any misconfiguration
            $test_conn = new mysqli ($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpass'], $_POST['dbname']);

            if ($test_conn->connect_errno) {

                $error_message = 'errno: ' . $test_conn->connect_errno . '<br/>';
                $error_message .= 'error: ' . $test_conn->connect_error . '<br/>';

                log_message('error', 'Unable to create DB Config File due to ' . $test_conn->connect_error);

                set_flash_message('danger', $error_message);
                redirect(SITEPATH);

            } else {

                //close the connection. since we have verify the connection is good.
                $test_conn->close();

                //Start creating db config file
                $filename = 'db.conf.php';
                $handle = fopen(BASEPATH . '/' . $filename, 'w') or die('Cannot open file:  '.$filename);

                $write = "<?php \n\n";
                $write .= "define('DBNAME', '" . $_POST['dbname'] . "');\n";
                $write .= "define('DBHOST', '" . $_POST['dbhost'] . "');\n";
                $write .= "define('DBUSER', '" . $_POST['dbuser'] . "');\n";
                $write .= "define('DBPASS', '" . $_POST['dbpass'] . "');\n";
                $write .= "\n\n?>";

                fwrite($handle, $write);
                fclose($handle);

                $report = $this->_setup_tables();

                $success_message = '';
                foreach ($report as $status) {
                    $success_message .= $status . '<br/>';
                }
                $success_message .= '<br/>';

                $success_message .= 'Great! Now we\'re good to go. You can start adding new Frog by selecting "Add Frog" from the menu on the left side';

                log_message('info', 'DB Config File created.');
                set_flash_message('success', $success_message);
                redirect(SITEPATH);

            }

        }
    }

    private function _setup_tables() {
        require('database.class.php');

        $db = new Database();

        $create_frog_table = "CREATE TABLE `frogs` (
            `frogid` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `frogname` varchar(255) DEFAULT NULL,
            `gender` char(1) DEFAULT NULL,
            `dob` date DEFAULT NULL,
            `habitatid` int(10) DEFAULT NULL,
            `photo` text,
            `alive` tinyint(1) DEFAULT '1',
            `datecreated` datetime DEFAULT NULL,
            `dateupdated` datetime DEFAULT NULL,
            PRIMARY KEY (`frogid`));";

            $create_habitat_table = "CREATE TABLE `habitat` (
            `habitatid` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `habitatname` varchar(255) DEFAULT NULL,
            `desc` text,
            `photo` text,
            `status` tinyint(1) DEFAULT '1' COMMENT '1 = Open , 0 = Close',
            `datecreated` datetime DEFAULT NULL,
            `dateupdated` datetime DEFAULT NULL,
            PRIMARY KEY (`habitatid`));";

            $insert_habitat = "INSERT INTO `habitat` (`habitatname`,`desc`,`datecreated`) VALUES ('Backyard', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec euismod justo, non rhoncus sapien. Nullam aliquam ex tellus, sit amet posuere arcu vestibulum id. Praesent at rhoncus felis. Phasellus efficitur suscipit velit ut elementum. Curabitur non elementum tellus, non imperdiet ipsum. Mauris gravida leo vitae cursus congue. Nulla viverra tincidunt ante, quis volutpat libero scelerisque ut. Suspendisse vulputate, nibh varius venenatis vestibulum, mi eros vulputate tortor, ac viverra elit felis mattis risus. Etiam quis rhoncus metus. Maecenas libero lacus, viverra vel odio et, iaculis tempor nisl. Ut ut efficitur lorem, at sagittis orci. Nullam dictum iaculis nunc non vestibulum. Pellentesque suscipit ligula ipsum, id interdum felis porttitor non.', NOW()), ('Wild', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec euismod justo, non rhoncus sapien. Nullam aliquam ex tellus, sit amet posuere arcu vestibulum id. Praesent at rhoncus felis. Phasellus efficitur suscipit velit ut elementum. Curabitur non elementum tellus, non imperdiet ipsum. Mauris gravida leo vitae cursus congue. Nulla viverra tincidunt ante, quis volutpat libero scelerisque ut. Suspendisse vulputate, nibh varius venenatis vestibulum, mi eros vulputate tortor, ac viverra elit felis mattis risus. Etiam quis rhoncus metus. Maecenas libero lacus, viverra vel odio et, iaculis tempor nisl. Ut ut efficitur lorem, at sagittis orci. Nullam dictum iaculis nunc non vestibulum. Pellentesque suscipit ligula ipsum, id interdum felis porttitor non.', NOW()), ('Aquarium', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec euismod justo, non rhoncus sapien. Nullam aliquam ex tellus, sit amet posuere arcu vestibulum id. Praesent at rhoncus felis. Phasellus efficitur suscipit velit ut elementum. Curabitur non elementum tellus, non imperdiet ipsum. Mauris gravida leo vitae cursus congue. Nulla viverra tincidunt ante, quis volutpat libero scelerisque ut. Suspendisse vulputate, nibh varius venenatis vestibulum, mi eros vulputate tortor, ac viverra elit felis mattis risus. Etiam quis rhoncus metus. Maecenas libero lacus, viverra vel odio et, iaculis tempor nisl. Ut ut efficitur lorem, at sagittis orci. Nullam dictum iaculis nunc non vestibulum. Pellentesque suscipit ligula ipsum, id interdum felis porttitor non.', NOW());";

            $insert_frog = "INSERT INTO `frogs` (`frogname`, `gender`, `dob`, `habitatid`, `datecreated`) VALUES ('James Bond', 'm', '2016-05-10', '1', NOW()), ('Scarlett Johansson', 'f', '2016-05-30', '1', NOW());";

        $mysqli = $db->get_conn_id();

        $db_setup = array();

        if ($mysqli->query($create_frog_table) === TRUE) {
            $t_frog = 'Table `frogs` has been created';
            $db_setup[] = $t_frog;
            log_message('info', $t_frog);

            if ($mysqli->query($create_habitat_table) === TRUE) {
                $t_habitat = 'Table `habitat` has been created';
                $db_setup[] = $t_habitat;
                log_message('info', $t_habitat);

                if ($mysqli->query($insert_habitat) === TRUE) {
                    $p_habitat = 'Table `habitat` has been pre-populated with sample data';
                    $db_setup[] = $p_habitat;
                    log_message('info', $p_habitat);

                    if ($mysqli->query($insert_frog) === TRUE) {
                        $p_frog = 'Table `frog` has been pre-populated with sample data';
                        $db_setup[] = $p_frog;
                        log_message('info', $p_frog);
                    } else {
                        $e_p_frog = "Error inserting data into table `frogs`: " . $mysqli->error;
                        $db_setup[] = $e_p_frog;
                        log_message('info', $e_p_frog);
                    }
                } else {
                    $e_p_habitat = "Error inserting data into table `habitat`: " . $mysqli->error;
                    $db_setup[] = $e_p_habitat;
                    log_message('info', $e_p_habitat);
                }
            } else {
                $e_habitat = "Error creating table `habitat`: " . $mysqli->error;
                $db_setup[] = $e_habitat;
                log_message('info', $e_habitat);
            }
        } else {
            $e_frog = "Error creating table `frogs`: " . $mysqli->error;
            $db_setup[] = $e_frog;
            log_message('info', $e_frog);
        }

        $db->close_db_conn();

        return $db_setup;


    }
}

?>