<?php
require_once('database.class.php');

/**
* Class: Frog
* CRUD methods for Frog
*/
class Frog {

    private static $_sqli;

    /**
     * Constructor
     */
    public function __construct() {
        if(empty(self::$_sqli)) {
            $db = new Database();
            self::$_sqli = $db->get_conn_id();
        }

    }

    /**
     * Destructor
     */
    public function __destruct() {
        if (is_resource(self::$_sqli)) {
            self::$_sqli->close();
        }
    }

    /**
     * Add Frog
     */
    public function add_frog() {

        $sqli = self::$_sqli;

        //remove submit button from the array
        unset($_POST['create']);

        if (isset($_FILES) && $_FILES['photo']['name'] != NULL) {
            $upload = upload_photo('frog');
        }

        $err_message = NULL;

        if (isset($upload) && $upload['status'] == FALSE) {
            $err_message .= $upload['message'];
        }

        array_filter($_POST, 'trim');
        $add = $_POST;

        $query  = " INSERT INTO `frogs` SET ";
        $query .=   "  `frogname` = '%s' ";
        $query .=   ", `gender` = '%s' ";
        $query .=   ", `dob` = '%s' ";
        $query .=   ", `habitatid` = %d ";
        $query .=   ", `photo` = '%s' ";
        $query .=   ", `datecreated` = NOW() ";

        $query = sprintf(
              $query
            , filter_var($add['name'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($add['gender'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($add['dob'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($add['habitatid'], FILTER_SANITIZE_NUMBER_INT)
            , filter_var($upload['filename'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to add ' . $add['name'] . '\'s record. <strong>' . $err_message . '</strong>');
            redirect(SITEPATH . '/form_frog.php');
        } else {
            if (! empty($err_message)) {
                set_flash_message('success', 'Yeay! ' . $add['name'] . ' has been added to Froggie\'s friend list. However, there is a problem while uploading '. $add['name'] . ' awesome photo due to <strong>' . $err_message . '</strong>');
            } else {
                set_flash_message('success', 'Yeay! ' . $add['name'] . ' has been added to Froggie\'s friend list.');
            }
            log_message('info', 'New frog created: ' . $add['name']);
            redirect(SITEPATH . '/manage_frog.php');
        }

    }

    /**
     * Update Frog
     * @param  Int $frogid Frog ID
     */
    public function update_frog($frogid) {

        $sqli = self::$_sqli;

        //remove submit button from the array
        unset($_POST['update']);

        if (isset($_FILES) && $_FILES['photo']['name'] != NULL) {
            $upload = upload_photo('frog');
        }

        $err_message = NULL;

        if (isset($upload) && $upload['status'] == FALSE) {
            $err_message .= $upload['message'];
        }

        array_filter($_POST, 'trim');
        $update = $_POST;

        $query  = " UPDATE `frogs` SET ";
        $query .=   "  `frogname` = '%s' ";
        $query .=   ", `gender` = '%s' ";
        $query .=   ", `dob` = '%s' ";
        $query .=   ", `habitatid` = %d ";
        $query .=   ", `alive` = ";
        $query .=   (isset($update['deceased'])) ? 0 : 1 ;

        if (isset($upload) && $upload['status'] == TRUE) $query .=   ", `photo` = '" . filter_var($upload['filename'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES) . "'";
        $query .=   ", `dateupdated` = NOW() ";
        $query .=   "WHERE `frogid` = %d ";

        $query = sprintf(
              $query
            , filter_var($update['name'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($update['gender'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($update['dob'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($update['habitatid'], FILTER_SANITIZE_NUMBER_INT)
            , filter_var($frogid, FILTER_SANITIZE_NUMBER_INT)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to update ' . $update['name'] . '\'s record. <strong>' . $err_message . '</strong>');
            redirect(SITEPATH . '/form_frog.php?fid=' . $frogid);
        } else {
            if (! empty($err_message)) {
                set_flash_message('success', 'Yeay! ' . $update['name'] . ' has been updated in Froggie\'s friend list. However, there is a problem while uploading '. $update['name'] . ' awesome photo due to <strong>' . $err_message . '</strong>');
            } else {
                set_flash_message('success', 'Yeay! ' . $update['name'] . ' has been updated in Froggie\'s friend list.');
            }
            log_message('info', 'Frog data updated: ' . $frogid);
            redirect(SITEPATH . '/manage_frog.php');
        }

    }

    /**
     * Get the list of frogs, with the option to filter by their habitat, and put it in a id => name array format
     * @param  Int $habitatid Default value is NULL. If passed, will become a filter for the frogs
     * @return Array $result An array of frogs, arranged in id => name format
     */
    public function get_list_frogs($habitatid = NULL) {
        $query = 'SELECT `frogid`, `frogname` FROM `frogs` ';
        if ($habitatid != NULL) $query .= 'WHERE `habitatid` = ' . filter_var($habitatid, FILTER_SANITIZE_NUMBER_INT);

        $result = array();

        $sqli = self::$_sqli;

        log_message('debug', 'Query : ' . $query);

        if ($results = $sqli->query($query)) {

            while ($row = $results->fetch_object()) {
                $result[$row->frogid] = $row->frogname;
            }
        } else {
            log_message('error', $sqli->error);
        }

        return $result;

    }

    /**
     * Get frogs full data, with an option to filter by their ID
     * @param  Int $frogid Default is NULL. If passed, it will become a filter parameter
     * @return Array $result An array of frogs data
     */
    public function get_frogs($frogid = NULL) {
        $sqli = self::$_sqli;

        $result = array();

        $query = "SELECT * FROM `frogs` ";
        if ($frogid != NULL) {
            $query .= " WHERE frogid = %d";

            $query = sprintf(
                  $query
                , filter_var($frogid, FILTER_SANITIZE_NUMBER_INT)
            );
        }
        log_message('debug', 'Query : ' . $query);

        if ($results = $sqli->query($query)) {

            while ($row = $results->fetch_object()) {
                if ($frogid != NULL) {
                    $result = (array) $row;

                } else {
                    $result[$row->frogid] = (array) $row;
                }
            }

        } else {
            log_message('error', $sqli->error);
        }

        $results->close();

        return $result;

    }

    /**
     * Delete frog record from table
     * @param  Int $frogid The Frog Id that will be deleted
     */
    public function delete_frog($frogid) {

        $sqli = self::$_sqli;

        $query  = " DELETE FROM `frogs` WHERE `frogid` = %d ";

        $query = sprintf(
              $query
            , filter_var($frogid, FILTER_SANITIZE_NUMBER_INT)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to remove the record.');
            redirect(SITEPATH . '/manage_frog.php');
        } else {
            log_message('info', 'Frog deleted : ' . $frogid);
            set_flash_message('success', 'Hmm.. Froggie\'s friend list gets smaller by one.');
            redirect(SITEPATH . '/manage_frog.php');
        }

    }

}

?>