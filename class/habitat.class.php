<?php
require_once('database.class.php');

/**
* Class: Habitat
* CRUD methods for Habitat
*/
class Habitat {

    private static $_sqli;

    /**
     * Constructor
     */
    public function __construct() {
        if(empty(self::$_sqli)) {
            $db = new Database();
            self::$_sqli = $db->get_conn_id();
        }

    }

    /**
     * Destructor
     */
    public function __destruct() {
        if (is_resource(self::$_sqli)) {
            self::$_sqli->close();
        }
    }

    /**
     * Add Habitat
     */
    public function add_habitat() {

        $sqli = self::$_sqli;

        //remove submit button from the array
        unset($_POST['create']);

        if (isset($_FILES) && $_FILES['photo']['name'] != NULL) {
            $upload = upload_photo('habitat');
        }

        $err_message = NULL;

        if (isset($upload) && $upload['status'] == FALSE) {
            $err_message .= $upload['message'];
        }

        array_filter($_POST, 'trim');
        $add = $_POST;

        $query  = " INSERT INTO `habitat` SET ";
        $query .=   "  `habitatname` = '%s' ";
        $query .=   ", `desc` = '%s' ";
        $query .=   ", `photo` = '%s' ";
        $query .=   ", `datecreated` = NOW() ";

        $query = sprintf(
              $query
            , filter_var($add['name'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($add['desc'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($upload['filename'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to add ' . $add['name'] . '\'s record. <strong>' . $err_message . '</strong>');
            redirect(SITEPATH . '/form_habitat.php');
        } else {
            if (! empty($err_message)) {
                set_flash_message('success', 'Yeay! ' . $add['name'] . ' has been added to Froggie\'s favourite Hangout place. However, there is a problem while uploading '. $add['name'] . ' awesome photo due to <strong>' . $err_message . '</strong>');
            } else {
                set_flash_message('success', 'Yeay! ' . $add['name'] . ' has been added to Froggie\'s favourite Hangout place.');
            }
            log_message('info', 'New Habitat created: ' . $add['name']);
            redirect(SITEPATH . '/manage_habitat.php');
        }

    }

    /**
     * Update Habitat
     * @param  Int $habitatid Habitat ID
     */
    public function update_habitat($habitatid) {

        $sqli = self::$_sqli;

        //remove submit button from the array
        unset($_POST['update']);

        if (isset($_FILES) && $_FILES['photo']['name'] != NULL) {
            $upload = upload_photo('habitat');
        }

        $err_message = NULL;

        if (isset($upload) && $upload['status'] == FALSE) {
            $err_message .= $upload['message'];
        }

        array_filter($_POST, 'trim');
        $update = $_POST;

        $query  = " UPDATE `habitat` SET ";
        $query .=   "  `habitatname` = '%s' ";
        $query .=   ", `desc` = '%s' ";
        $query .=   ", `status` = ";
        $query .=   (isset($update['closed'])) ? 0 : 1 ;

        if (isset($upload) && $upload['status'] == TRUE) $query .=   ", `photo` = '" . filter_var($upload['filename'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES) . "'";
        $query .=   ", `datecreated` = NOW() ";
        $query .=   "WHERE `habitatid` = %d ";

        $query = sprintf(
              $query
            , filter_var($update['name'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($update['desc'], FILTER_SANITIZE_STRING | FILTER_SANITIZE_MAGIC_QUOTES)
            , filter_var($habitatid, FILTER_SANITIZE_NUMBER_INT)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to update ' . $update['name'] . '\'s record. <strong>' . $err_message . '</strong>');
            redirect(SITEPATH . '/form_habitat.php?hid=' . $habitatid);
        } else {
            if (! empty($err_message)) {
                set_flash_message('success', 'Yeay! ' . $update['name'] . ' has been updated in Froggie\'s favourite Hangout place. However, there is a problem while uploading '. $update['name'] . ' awesome photo due to <strong>' . $err_message . '</strong>');
            } else {
                set_flash_message('success', 'Yeay! ' . $update['name'] . ' has been updated in Froggie\'s favourite Hangout place.');
            }
            log_message('info', 'Habitat data updated: ' . $habitatid);
            redirect(SITEPATH . '/manage_habitat.php');
        }

    }

    /**
     * Get the list of Habitat, and put it in a id => name array format
     * @return Array $result An array of habitat, arranged in id => name format
     */
    public function get_list_habitat() {
        $query = 'SELECT `habitatid`, `habitatname` FROM `habitat`';

        $result = array();

        $sqli = self::$_sqli;

        log_message('debug', 'Query: ' . $query);

        if ($results = $sqli->query($query)) {

            while ($row = $results->fetch_object()) {
                $result[$row->habitatid] = $row->habitatname;
            }
        } else {
            log_message('error', $sqli->error);
        }

        return $result;

    }

    /**
     * Get Habitat full data, with an option to filter by their ID
     * @param  Int $habitatid Default is NULL. If passed, it will become a filter parameter
     * @return Array $result An array of Habitat data
     */
    public function get_habitat($habitatid = NULL) {
        $sqli = self::$_sqli;

        $result = array();

        $query = "SELECT * FROM `habitat` ";
        if ($habitatid != NULL) {
            $query .= " WHERE habitatid = %d";

            $query = sprintf(
                  $query
                , filter_var($habitatid, FILTER_SANITIZE_NUMBER_INT)
            );
        }

        log_message('debug', 'Query: ' . $query);

        if ($results = $sqli->query($query)) {

            while ($row = $results->fetch_object()) {
                if ($habitatid) {
                    $result = (array) $row;
                } else {
                    $result[$row->habitatid] = (array) $row;
                }
            }

        } else {
            log_message('error', $sqli->error);
        }

        $results->close();

        return $result;

    }


    /**
     * Delete habitat record from table
     * @param  Int $habitatid The Habitat Id that will be deleted
     */
    public function delete_habitat($habitatid) {

        $sqli = self::$_sqli;

        $query  = " DELETE FROM `habitat` WHERE `habitatid` = %d ";

        $query = sprintf(
              $query
            , filter_var($habitatid, FILTER_SANITIZE_NUMBER_INT)
        );

        log_message('debug', 'Query: ' . $query);

        $result = $sqli->query($query);

        if(! $result) {

            log_message('error', $sqli->error);
            set_flash_message('danger', 'Oops! Froggie encountered some error while trying to remove the record.');
            redirect(SITEPATH . '/manage_habitat.php');
        } else {

            log_message('info', 'Habitat deleted : ' . $habitatid);
            set_flash_message('success', 'Hmm.. There goes another Froggie\'s chillin\' spot.');
            redirect(SITEPATH . '/manage_habitat.php');
        }

    }

}

?>