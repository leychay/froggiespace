<?php
require_once('db.conf.php');


/**
 *
 * Class: Database
 *
 */
class Database {


    static private $_resource_id = NULL;

    /**
     *
     * Constructor
     *
     */
    public function __construct($no_reconnect = FALSE) {
        if($no_reconnect == FALSE) {
            $this->open_db_conn();
        }
    }

    /**
     *
     * function get_conn_id
     *
     * @return  resource
     */
    public function get_conn_id() {
        return self::$_resource_id;
    }

    /**
     *
     * function open_db_conn
     *
     */
    public function open_db_conn() {
        if(empty(self::$_resource_id)) {
            self::$_resource_id = new mysqli (DBHOST, DBUSER, DBPASS, DBNAME)
            or die('Could not connect to Database Server');
        }
    }
    /**
     *
     * function close_db_conn
     *
     */
    public function close_db_conn() {
    	self::$_resource_id->close();
        self::$_resource_id = NULL;
    }
}