<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/init.class.php');

$init = new Init();

if (isset($_POST['create'])) {
    $status = $init->create_db_conf();
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Home</h1>
    </div>
</div>

<?php echo flash_message(); ?>

<div class="row">
    <div class="col-lg-12">
        <?php
        $db_conf = $init->check_db_conf();

        if (! $db_conf['status']) {

            echo $db_conf['message'];
        ?>

        <div class="row">
            <div class="col-lg-6">
                <h3>Database Connection setup</h3>
                <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" role="form" class="validate">

                    <div class="form-group">
                        <label for="dbname">* Database Name</label>
                        <input type="text" id="dbname" name="dbname" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Database Name" placeholder="e.g: frogpond" value="" required  autofocus/>
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="dbhost">* Database Host</label>
                        <input type="text" id="dbhost" name="dbhost" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Database Host. e.g: localhost, or localhost:3306" placeholder="e.g: localhost, or localhost:3306" value="localhost" required />
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="dbuser">* Database User</label>
                        <input type="text" id="dbuser" name="dbuser" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Database User" placeholder="e.g: fpuser" value="" required />
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="dbpass">Database Password</label>
                        <input type="password" id="dbpass" name="dbpass" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Database Password. Leave blank for no password" value="" />
                        </label>
                    </div>

                    <div class="form-group">
                        <input type="submit" id="create" name="create" class="btn btn-primary" value="Create DB Config" />
                        </label>
                    </div>

                </form>
                <hr />
                <div class="alert alert-info"> All field marked with (*) are mandatory</div>

            </div>
        </div>

        <?php
        } else {

            echo $db_conf['message'];
        }
        ?>
    </div>
</div>

<?php require_once('include/footer.php'); ?>
