<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/frog.class.php');
require_once('class/habitat.class.php');

if (isset($_GET['fid'])) {
    $is_update = TRUE;
    $frogid = $_GET['fid'];
} else {
    $is_update = FALSE;
}

$CFrog = new Frog();
$CHabitat = new Habitat();

//set default value for habitat
$habitat = array(NULL => 'Please Select');
//now merge it with the data from habitat table
$habitat += $CHabitat->get_list_habitat();

//Add the record
if (isset($_POST['create'])) {
    $CFrog->add_frog();
}

//update the record
if (isset($_POST['update'])) {
    $CFrog->update_frog($frogid);
}

if ($is_update) {
    //get frog detail using ID
    $frog = $CFrog->get_frogs($frogid);

    //redirect to Manage page if no record
    if (empty($frog)) {
        set_flash_message('info', 'That frog is not exists in Froggie\'s List');
        redirect('manage_frog.php');
    }

}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo ($is_update) ? 'Update Frog Information' : 'Add New Frog' ; ?></h1>
    </div>
</div>

<?php echo flash_message(); ?>

<form method="POST" action="<?php echo ($is_update) ? $_SERVER['PHP_SELF'] . '?fid=' . $frogid : $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" role="form" class="validate">
<div class="row">
    <div class="col-lg-6">

        <div class="form-group">
            <label for="name">Frog Name</label>
            <input id="name" type="text" name="name" class="form-control" data-toggle="tooltip" data-placement="top" title="Enter Frog Name" value="<?php echo ($is_update) ? $frog['frogname'] : NULL ; ?>" required autofocus />
        </div>

        <div class="form-group">
            <label for="gender">Gender</label>
            <select id="gender" name="gender" class="form-control" data-toggle="tooltip" data-placement="top" title="Select Frog gender" required>
                <option value="m" <?php echo ($is_update && $frog['gender'] == 'm') ? 'selected' : NULL; ?>>Male</option>
                <option value="f" <?php echo ($is_update && $frog['gender'] == 'f') ? 'selected' : NULL; ?>>Female</option>
            </select>
        </div>
        <div class="form-group">
            <label for="dob">Date of Birth</label>
            <input id="dob" type="text" name="dob" class="form-control datepicker" data-toggle="tooltip" data-placement="top" title="Select Date of Birth" value="<?php echo ($is_update) ? $frog['dob'] : NULL ; ?>" readonly required />
        </div>

        <div class="form-group">
            <label for="habitat">Habitat</label>
            <select id="habitat" name="habitatid" class="form-control" data-toggle="tooltip" data-placement="top" title="Select Habitat" required>
                <?php
                foreach ($habitat as $id => $val) {
                    if ($is_update) {
                        if ($frog['habitatid'] == $id) {
                            $selected = 'selected';
                        } else {
                            $selected = NULL;
                        }
                        echo '<option value="' . $id . '"' . $selected . '>' . $val . '</option>';
                    } else {
                        echo '<option value="' . $id . '">' . $val . '</option>';
                    }
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <?php
            if ($is_update) {
            ?>
            <div class="form-group">
            <div class="checkbox">
                    <label for="deceased">
                        <input id="deceased" type="checkbox" name="deceased" <?php echo (! $frog['alive']) ? 'checked' : NULL ; ?> />Deceased
                    </label>
                </div>
            </div>
            <input id="update" type="submit" name="update" value="update this Frog" class="btn btn-primary"  />


            <?php
            } else {
            ?>
            <input id="create" type="submit" name="create" value="Add New Frog!" class="btn btn-primary"  />
            <?php
            }
            ?>
        </div>
    </div>

    <div class="col-lg-6">

        <div class="form-group">
        <label for="photo">Upload photo</label>
        <input id="photo" type="file" name="photo" data-toggle="tooltip" data-placement="top" title="Select photo for upload" />
        <p class="help-block"><small>(only .jpg or .png is allowed) | (Maximum File Size is <?php echo ini_get('upload_max_filesize');?>)</small></p>
        </div>

        <?php if ($is_update && ! empty($frog['photo'])) { ?>
        <div class="row text-center">
            <div class="col-md-6">
                <div class="panel panel-primary">
                <div class="panel-body">
                    <img width="200px" class="thumb" src="<?php echo ASSETPATH . 'img/' . $frog['photo']; ?>" />
                </div>
                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
</form>

<?php require_once('include/footer.php'); ?>
