<?php

/**
 * Helper.php
 * This page contains all helper functions that can be called anywhere within the pages,
 * without instantiating a Class
 */


/**
 * Set Flash Message: set flash message to session
 * @param String $type Type of Flash Message to be displayed
 *          Types: success, danger, info, warning
 * @param String $message Content of Flash Message to be displayed
 */
if (! function_exists('set_flash_message')) {
    function set_flash_message($type, $message) {
        $_SESSION['flash']['flash_type'] = $type;
        $_SESSION['flash']['flash_message'] = $message;

    }
}

/**
 * Flash Message: flash session message that are passed in the $_SESSION['flash'] variable
 * Checks if there's a flash item in $_SESSION, flash it based on its type color
 * @return String $flash Returns an HTML code of flash message that has been set by its type
 */
if (! function_exists('flash_message')) {
    function flash_message(){

        if (isset($_SESSION['flash'])) {
            $flash = '<div class="row">';
            $flash .= '<div class="col-lg-12">';
            $flash .= '<div class="alert alert-' . $_SESSION['flash']['flash_type'] . ' alert-dismissable">';
            $flash .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            $flash .= '<p>' . $_SESSION['flash']['flash_message'] . '</p>';
            $flash .= '</div>';
            $flash .= '</div>';
            $flash .= '</div>';
            unset($_SESSION['flash']);
        } else {
            $flash = NULL;
        }

        return $flash;

    }
}
/**
 * Redirect: redirect to the specified URL
 * @param String $url The URL for the redirection
 */
if (! function_exists('redirect')) {
    function redirect($url) {
        header("Location:" . $url, 302);
        exit();
    }
}

/**
 * Upload Photo: Basic function for uploading and validating the uploads
 * @param String $entity The entity representative of the uploaded file. This will be appended to the file name
 * @return Array $status If the was an error during the upload, return the status as FALSE, and the reason of the error
 */
if (! function_exists('upload_photo')) {
    function upload_photo($entity = NULL) {

        if (! empty($_FILES)) {

            $result = array();
            // Check for Filesize limit. basic checking
            if ($_FILES['photo']['error'] == UPLOAD_ERR_FORM_SIZE || $_FILES['photo']['error'] == UPLOAD_ERR_INI_SIZE) {

                $result['status'] = FALSE;
                $result['filename'] = NULL;
                $result['message'] = 'Exceeded file size limit.';

            } else {

                //Once the limit is below limit, proceed with mime types and validation
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $_FILES['photo']['tmp_name']);
                finfo_close($finfo);

                $allowed = array('jpg' => 'image/jpeg','png' => 'image/png');

                if (FALSE === $ext = array_search($mime,$allowed, TRUE)) {

                    $result['status'] = FALSE;
                    $result['filename'] = NULL;
                    $result['message'] = 'Invalid file format.';

                } else {

                    //Now do the final validation, and move the file to our fixed location
                    $img_path =  BASEPATH . '/assets/img/';
                    $file_name = $entity . '_' . basename($_FILES['photo']['name']);
                    $file = $img_path . $file_name;

                    if (move_uploaded_file($_FILES['photo']['tmp_name'], $file)) {

                        $result['status'] = TRUE;
                        $result['filename'] = $file_name;
                        $result['message'] = 'Photo successfully uploaded.';

                    } else {

                        $result['status'] = FALSE;
                        $result['filename'] = NULL;
                        $result['message'] = 'Invalid type of file. Upload aborted.';

                    }

                }

            }

            return $result;

        }
    }
}

/**
 * truncate string : to truncate any given string based on the size set, and append ellipsis at the end of the string
 * @param  String  $string The string to be truncated
 * @param  Integer $size   The size/length of the truncated string. default is set at 100
 * @return String          The truncated string
 */
if (! function_exists('truncate_string')) {
    function truncate_string($string, $size = 100) {
        if (! empty($string)) {
            if (strlen($string) > $size) {
                return substr_replace($string, '...', $size);
            } else {
                return $string;
            }
        }
    }
}

/**
 * log_message: log error/warning/debug/info message
 * @param  String $type type of log message that will be log. error | warning | debug | info
 * @param  String $msg message that will be logged
 */
if (! function_exists('log_message')) {
    function log_message($type, $msg){

        $date = date('Y-m-d');
        $file = 'log_' . $date . '.log';

        $fp = fopen(BASEPATH . '/logs/' . $file, 'a');

        $log_message = date('[Y-m-d H:i:s]') . ' ';
        $log_message .= $type . ' --> ' . $msg . "\n";

        fwrite($fp, $log_message);
        fclose($fp);

    }
}


?>