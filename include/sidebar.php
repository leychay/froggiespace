        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">FroggieSpace</a>
            </div>
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-angle-right fa-fw"></i> Home</a>
                        </li>
                        <?php if (file_exists('db.conf.php')) { ?>
                        <li>
                            <a href="form_frog.php"><i class="fa fa-angle-right fa-fw"></i> Add Frog</a>
                        </li>
                        <li>
                            <a href="form_habitat.php"><i class="fa fa-angle-right fa-fw"></i> Add Habitat</a>
                        </li>
                        <li>
                            <a href="manage_frog.php"><i class="fa fa-angle-right fa-fw"></i> Manage Frogs</a>
                        </li>
                        <li>
                            <a href="manage_habitat.php"><i class="fa fa-angle-right fa-fw"></i> Manage Habitat</a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">