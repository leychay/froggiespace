            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<script type="text/javascript">
$(document).ready( function() {
    $(document).tooltip({
        selector: "[data-toggle=tooltip]"
    });

    $('.validate').validate();

    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.table').DataTable();

});
</script>

</body>

</html>
<?php ob_end_flush(); ?>