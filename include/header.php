<?php require_once('common.conf.php'); ?>
<?php require_once('include/helper.php'); ?>
<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FrogPond</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo ASSETPATH ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo ASSETPATH ?>metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo ASSETPATH ?>css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo ASSETPATH ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery UI -->
    <link href="<?php echo ASSETPATH ?>jquery-ui/themes/base/jquery-ui.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="<?php echo ASSETPATH ?>jquery/dist/jquery.min.js"></script>

    <!-- jQuery UI validation -->
    <script src="<?php echo ASSETPATH ?>jquery-ui/jquery-ui.min.js"></script>

    <!-- jQuery validation -->
    <script src="<?php echo ASSETPATH ?>jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ASSETPATH ?>bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo ASSETPATH ?>metisMenu/dist/metisMenu.min.js"></script>

    <!-- Data Tables Core -->
    <script type="text/javascript" src="<?php echo ASSETPATH ?>datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETPATH ?>datatables/media/js/dataTables.bootstrap.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo ASSETPATH ?>js/sb-admin-2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

