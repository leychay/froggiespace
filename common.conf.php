<?php
//disable error/warning that are thrown by PHP (production setting)
error_reporting(0);

//start session
session_start();

define('BASEPATH', dirname(__FILE__));
define('SITEPATH', 'http://' . $_SERVER['SERVER_NAME']);
define('ASSETPATH', SITEPATH . '/assets/');

?>