<?php
require_once('include/header.php');
require_once('include/sidebar.php');

require_once('class/database.class.php');
require_once('class/habitat.class.php');


$db = new Database();

$CHabitat = new Habitat();

$habitat = $CHabitat->get_habitat();

//get the frog id for deletion
if (isset($_GET['dhid'])) {
    $CHabitat->delete_habitat($_GET['dhid']);
}

?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Manage Habitat</h1>
    </div>
</div>

<?php echo flash_message(); ?>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" role="form">
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
            <thead>
                <th>Name</th>
                <th>Description</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
            </thead>
            <tbody>
                <?php if (! empty($habitat)){ ?>
                    <?php foreach($habitat as $hbt){ ?>
                    <tr <?php echo (! $hbt['status']) ? 'style="text-decoration:line-through;"' : NULL ; ?>>
                        <td><?php echo $hbt['habitatname']; ?></td>
                        <td><?php echo truncate_string($hbt['desc']); ?></td>
                        <td class="text-center">
                            <?php
                            if ($hbt['status']) {
                                echo '<label class="btn btn-success btn-xs">Open</label>';
                            } else {
                                echo '<label class="btn btn-danger btn-xs">Closed</label>';
                            }
                            ?>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li class="text-center"><a href="habitat.php?hid=<?php echo $hbt['habitatid'] ?>">View Habitat</a></li>
                                    <li class="text-center"><a href="form_habitat.php?hid=<?php echo $hbt['habitatid'] ?>">Update Info</a></li>
                                    <li class="text-center"><a class='btn-danger delete_habitat' href="manage_habitat.php?dhid=<?php echo $hbt['habitatid'] ?>">Froggie Hate'em</a></li>
                                </ul>
                            </div>

                        </td>
                    </tr>
                <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">No Habitat available!</td>
                    </tr>
                <?php } ?>
            </tbody>
            </table>
        </div>
    </div>
</form>

<script type="text/javascript">
$(function() {

    $('.delete_habitat').click( function() {
        if (confirm("Help Froggie to remove this place from his favorite list?")) {
            return true;
        } else {
            return false;
        }
    } );
});

</script>
<?php require_once('include/footer.php'); ?>
